package com.example.joycelindchow.androidcw;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import static com.example.joycelindchow.androidcw.AddActivity.gallery_Pick;

public class EditListActivity extends AppCompatActivity {
    private static final String TAG = "EditListActivity";
    String whyError = "";
    //widgets
    private EditText editTextName;
    private EditText editTextStudentID;
    private EditText editTextEmail;
    private EditText editTextCohort;
    private CircularImageView imageViewProfile;
    private DatabaseReference studentData;
    private StorageReference studentProfile;
    private Student student;
    private Uri imageUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //get intent from list activity
        student = (Student) getIntent().getSerializableExtra("student");
        //initialize the Firebase object
        studentData = FirebaseDatabase.getInstance().getReference("studentList");
        studentProfile = FirebaseStorage.getInstance().getReference("profile Images");
        showUpdateDialog();
    }

    private void showUpdateDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.edit_list_dialog, null);
        dialogBuilder.setView(dialogView);

        //initialize widgets
        editTextName = dialogView.findViewById(R.id.editText_EditList_editName);
        editTextStudentID = dialogView.findViewById(R.id.editText_EditList_editstudentID);
        editTextEmail = dialogView.findViewById(R.id.editText_EditList_editEmail);
        editTextCohort = dialogView.findViewById(R.id.editText_EditList_editCohort);
        imageViewProfile = dialogView.findViewById(R.id.imageView__EditList_profileImage);
        Button buttonUpdate = dialogView.findViewById(R.id.button_EditList_updateButton);
        Button buttonCancel = dialogView.findViewById(R.id.button_EditList_cancelButton);
        Button buttonDelete = dialogView.findViewById(R.id.button_EditList_deleteButton);

        //get the edit text from the serializable
        editTextName.setText(student.getName());
        editTextStudentID.setText(student.getStudentID());
        editTextEmail.setText(student.getEmail());
        editTextCohort.setText(student.getCohort());
        Picasso.get().load(student.getImage()).into(imageViewProfile);

        dialogBuilder.setTitle("Update Student");
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        //set button update to click listener
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = editTextName.getText().toString().trim();
                String newStudentID = editTextStudentID.getText().toString().trim();
                String newEmail = editTextEmail.getText().toString().trim();
                String newCohort = editTextCohort.getText().toString().trim();

                if (validate(newName, newStudentID, newEmail, newCohort)) {
                    updateStudent(newName, newStudentID, newEmail, newCohort);
                }
                alertDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                startActivity(intent);
            }
        });

        //set button cancel to click listener
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                startActivity(intent);
            }
        });

        //set button delete to click listener
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteStudent();
                Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                startActivity(intent);
            }
        });

        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, gallery_Pick);
            }
        });
    }

    private void updateStudent(String newName, String newStudentID, String newEmail, String newCohort) {
        //updating student
        HashMap<String, Object> hashMapUpdate = new HashMap<>();
        String id = student.getId();
        String newImage = imageUri.toString().trim();

        hashMapUpdate.put("id",id);
        hashMapUpdate.put("name", newName);
        hashMapUpdate.put("studentID",newStudentID);
        hashMapUpdate.put("email", newEmail);
        hashMapUpdate.put("cohort",newCohort);
        hashMapUpdate.put("image",newImage);
        studentData.child(id).updateChildren(hashMapUpdate);

        Toast.makeText(getApplicationContext(), "Student Updated", Toast.LENGTH_SHORT).show();
    }

    private void deleteStudent() {
        String id = student.getId();
        studentData.child(id).removeValue();
        Toast.makeText(getApplicationContext(), "Student Deleted", Toast.LENGTH_SHORT).show();
    }

    private boolean validate(String name, String studentID, String email, String cohort) {
        return checkIfDataNotBlank(name, studentID, email, cohort);
    }

    public boolean checkIfDataNotBlank(String name, String studentID, String email, String cohort) {
        boolean result = true;
        if (name.equals("") || studentID.equals("") || email.equals("") || cohort.equals("")) {
            whyError = "Please fill all the fields";
            result = false;
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String id = studentData.push().getKey();
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == gallery_Pick && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();

            //cropping image tools
            //start picker to get image for cropping and then use image in cropping activity
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            //if the result is ok
            if (resultCode == RESULT_OK) {
                //then get the cropped image
                final Uri resultUri = result.getUri();
                //store the profile image by their using their id and in the form of jpg
                final StorageReference filePath = studentProfile.child(id);
                Picasso.get().load(resultUri).into(imageViewProfile);
                //save the crop image in the firebase storage, to see if the image is successfully saved
                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            Picasso.get().load(resultUri).into(imageViewProfile);
                            Toast.makeText(EditListActivity.this, "Image updated successfully",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                Toast.makeText(this, "Error Occured: Image can't be cropped. Try Again.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}