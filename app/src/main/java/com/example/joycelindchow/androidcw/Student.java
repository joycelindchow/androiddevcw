package com.example.joycelindchow.androidcw;

import android.net.Uri;

import java.io.Serializable;

//serializable to pass object by intent
public class Student implements Serializable {
    private String id;
    private String name;
    private String studentID;
    private String email;
    private String cohort;
    private String image;

    private static final String TAG = "ListConstructor";

    public Student(String id, String name, String studentID, String email, String cohort, String image) {
        this.id = id;
        this.name = name;
        this.studentID = studentID;
        this.email = email;
        this.cohort = cohort;
        this.image = image;
    }

    public Student () {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCohort() {
        return cohort;
    }

    public void setCohort(String cohort) {
        this.cohort = cohort;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
