package com.example.joycelindchow.androidcw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.StudentViewHolder> implements Filterable {
    private Context context;
    private List<Student> studentList;
    private List<Student> studentListFull;

    public ListAdapter(Context context, List<Student> studentList) {
        this.context = context;
        this.studentList = studentList;
        studentListFull= new ArrayList<>(studentList);
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StudentViewHolder(
                LayoutInflater.from(context).inflate(R.layout.list_adapter, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.StudentViewHolder holder, int position) {
        Student student = studentList.get(position);
        holder.textViewName.setText(student.getName());
        holder.textViewStudentID.setText(student.getStudentID());
        holder.textViewEmail.setText(student.getEmail());
        holder.textViewCohort.setText(student.getCohort());
        holder.setImageProfile(context, student.getImage());
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    @Override
    public Filter getFilter() {
        return listFiler;
    }

    private Filter listFiler = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Student> filteredList = new ArrayList<>();
            if(constraint == null || constraint.length() == 0) {
                filteredList.addAll(studentListFull);
            } else {
                //search input
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(Student item: studentListFull) {
                    if(item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            studentList.clear();
            studentList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

    class StudentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewName;
        TextView textViewStudentID;
        TextView textViewEmail;
        TextView textViewCohort;
        CircularImageView imageViewProfile;

        public StudentViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textView_Name);
            textViewStudentID = itemView.findViewById(R.id.textView_studentID);
            textViewEmail = itemView.findViewById(R.id.textView_email);
            textViewCohort = itemView.findViewById(R.id.textView_cohort);
            imageViewProfile = itemView.findViewById(R.id.imageView_profileImage);

            itemView.setOnClickListener(this);
        }

        public void setImageProfile(Context ctx, String image){
            ImageView Image = itemView.findViewById(R.id.imageView_profileImage);
            Picasso.get().load(image).into(Image);
        }

        @Override
        public void onClick(View v) {
            Student student = studentList.get(getAdapterPosition());
            Intent intent = new Intent(context, EditListActivity.class);
            intent.putExtra("student", student);
            context.startActivity(intent);
        }
    }
}
