package com.example.joycelindchow.androidcw;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {
    final static int gallery_Pick = 1;
    private static final String TAG = "AddActivity";
    String whyError = "";
    private EditText editText_fullName;
    private EditText editText_studentID;
    private EditText editText_email;
    private EditText editText_cohort;
    private Button button_save;
    private Button button_cancel;
    private CircularImageView imageView_profile;

    private StorageReference studentProfile;
    private DatabaseReference studentData;

    private Uri imageUri;


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void establish() {
        editText_fullName = findViewById(R.id.editText_ListActivity_name);
        editText_studentID = findViewById(R.id.editText_ListActivity_studentID);
        editText_email = findViewById(R.id.editText_ListActivity_email);
        editText_cohort = findViewById(R.id.editText_ListActivity_cohort);
        button_save = findViewById(R.id.button_ListActivity_save);
        button_cancel = findViewById(R.id.button_ListActivity_cancel);
        imageView_profile = findViewById(R.id.imageView__ListActivity_profileImage);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        establish();

        studentData = FirebaseDatabase.getInstance().getReference("studentList");
        studentProfile = FirebaseStorage.getInstance().getReference().child("profile Images");
        Student student = (Student) getIntent().getSerializableExtra("student");

        //click listener to the button
        button_save.setOnClickListener(this);

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        //intent the user to their phone gallery
        imageView_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, gallery_Pick);
            }
        });
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: started");

        //retrieve information
        String name = editText_fullName.getText().toString().trim();
        String studentID = editText_studentID.getText().toString().trim();
        String email = editText_email.getText().toString().trim();
        String cohort = editText_cohort.getText().toString().trim();
        String image = imageUri.toString().trim();

        //validate input
        if (validate(name, studentID, email, cohort)) {
            String id = studentData.push().getKey();

            //constructor
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("id", id);
            hashMap.put("name", name);
            hashMap.put("studentID", studentID);
            hashMap.put("email", email);
            hashMap.put("cohort", cohort);
            hashMap.put("image", image);

            //add to database
            studentData.child(id).updateChildren(hashMap);
            Toast.makeText(this, "Student Added", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), ListActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Failed to add student", Toast.LENGTH_LONG).show();
        }
    }

    private boolean validate(String name, String studentID, String email, String cohort) {
        return checkIfDataNotBlank(name, studentID, email, cohort);
    }

    public boolean checkIfDataNotBlank(String name, String studentID, String email, String cohort) {
        boolean result = true;
        if (name.equals("") || studentID.equals("") || email.equals("") || cohort.equals("")) {
            whyError = "Please fill all the fields";
            result = false;
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String id = studentData.push().getKey();
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == gallery_Pick && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();

            //cropping image tools
            //start picker to get image for cropping and then use image in cropping activity
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            //if the result is ok
            if (resultCode == RESULT_OK) {
                //then get the cropped image
                final Uri resultUri = result.getUri();
                //store the profile image by their using their id and in the form of jpg
                final StorageReference filePath = studentProfile.child(id + ".jpg");
                Picasso.get().load(resultUri).into(imageView_profile);
                //save the crop image in the firebase storage, to see if the image is successfully saved
                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            Picasso.get().load(resultUri).into(imageView_profile);
                        }
                    }
                });
            } else {
                Toast.makeText(this, "Error Occured: Image can't be cropped. Try Again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}